import numpy as np
import keras_network
from keras import backend as K
from utils import separator,max_gen


def pruning_layerwise(model, layer_index, node_number, data = None, retrain_epoch = 20,
                      quantity_list = [1,2,3,4,5,6,7], do_without_bias = True,
                      do_with_bias = True, do_retraining = False,
                      saved_data = None, inversed_quantity=False):
    [training_data, validation_data, test_data] = data
    if saved_data == None:
        y1=np.zeros((20,node_number))
        y2=np.zeros((20,node_number))
        y3=np.zeros((20,node_number))
    else:
        [y1, y2, y3, x] = saved_data


    if 19 in quantity_list:
        data_free_without_merge = True
        quantity_list.remove(19)
    else:
        data_free_without_merge = False

    if 20 in quantity_list:
        data_free_with_merge = True
        quantity_list.remove(20)
    else:
        data_free_with_merge = False


    net = keras_network.Network([])
    for j in quantity_list:
        accu1=[]
        accu2=[]
        accu3=[]
        for i in range(node_number):
            if do_without_bias:
                net.load_model(model)
                accu1.append(net.prune_node_layerwise(test_data,layer_index,i,j,\
                        inversed_quantity=inversed_quantity))
                K.clear_session()
            if do_with_bias:
                net.load_model(model)
                accu2.append(net.prune_node_layerwise(test_data,layer_index,i,
                             j,do_bias_balance=True,
                             inversed_quantity=inversed_quantity))
                K.clear_session()
            if do_retraining:
                net.load_model(model)
                net.prune_node_layerwise(test_data,layer_index,i,j,
                        do_bias_balance=True,
                        inversed_quantity=inversed_quantity)
                net.SGD(training_data, retrain_epoch, 32, 0.5, lmbda=5.0,
                        evaluation_data=validation_data,starting_layer = 1)
                lost,accuracy=net.model.evaluate(test_data[0],test_data[1])
                accu3.append(accuracy)
                K.clear_session()

        if do_without_bias:
            y1[j-1,:]=accu1
        if do_with_bias:
            y2[j-1,:]=accu2
        if do_retraining:
            y3[j-1,:]=accu3

    accu4 = [y2[1,0]]
    accu5 = [y2[1,0]]
    if data_free_without_merge:
        net.load_model(model)
        accu4=accu4+net.merge_node(layer_index,node_number-1,surgery = False,
                validation_data = validation_data)
        y1[18,:]=accu4
    K.clear_session()

    if data_free_with_merge:
        net.load_model(model)
        accu5=accu5+net.merge_node(layer_index,node_number-1,surgery = True,
                validation_data = validation_data)
        y1[19,:]=accu5



    K.clear_session()
    x=np.arange(0,node_number)
    return y1,y2,y3,x


def pruning_layers_evenly(model, node_number, data = None, retrain_epoch = 20,
                      quantity_list = [1,2,7,13], do_without_bias = True,
                      do_with_bias = True, do_retraining = False):
    [training_data, validation_data, test_data] = data
    number_of_hidden_layer = 2
    y1=np.zeros((20,node_number))
    y2=np.zeros((20,node_number))
    y3=np.zeros((20,node_number))
    y4=np.zeros((20,node_number))
    y5=np.zeros((20,node_number))
    y6=np.zeros((20,node_number))
    net = keras_network.Network([])
    for j in quantity_list:
        accu1=[]
        accu2=[]
        accu3=[]
        accu4=[]
        accu5=[]
        accu6=[]
        for i in range(node_number):
            if do_without_bias:
                net.load_model(model)
                n1,n2 = max_gen(net.quantity_idx_dict[j][1][:],
                        net.quantity_idx_dict[j][2][:],i*2)
                net.prune_node_layerwise(test_data,1,i,j,do_bias_balance=False)
                accu1.append(net.prune_node_layerwise(test_data,
                             2,i,j,do_bias_balance=False))
                K.clear_session()

                net.load_model(model)
                net.prune_node_layerwise(test_data,1,len(n1),j,
                        do_bias_balance=False)
                accu4.append(net.prune_node_layerwise(test_data,2,len(n2),j,
                    do_bias_balance=False))
                K.clear_session()
            if do_with_bias:
                net.load_model(model)
                n1,n2 = max_gen(net.quantity_idx_dict[j][1][:],
                        net.quantity_idx_dict[j][2][:],i*2)
                net.prune_node_layerwise(test_data,1,i,
                        j,do_bias_balance=True)
                accu2.append(net.prune_node_layerwise(test_data,
                    2,i,j,do_bias_balance=True))
                K.clear_session()

                net.load_model(model)
                net.prune_node_layerwise(test_data,1,len(n1),j,
                        do_bias_balance=True)
                accu5.append(net.prune_node_layerwise(test_data,2,len(n2),j,
                    do_bias_balance=True))
                K.clear_session()
            if do_retraining:
                net.load_model(model)
                n1,n2 = max_gen(net.quantity_idx_dict[j][1][:],
                        net.quantity_idx_dict[j][2][:],i*2)
                net.prune_node_layerwise(test_data,1,i,
                        j,do_bias_balance=True)
                net.prune_node_layerwise(test_data,
                    2,i,j,do_bias_balance=True)
                net.SGD(training_data, retrain_epoch, 32, 0.5, lmbda=5.0,
                        evaluation_data=test_data,starting_layer = 1)
                lost,accuracy=net.model.evaluate(test_data[0],test_data[1])
                accu3.append(accuracy)
                K.clear_session()

                net.load_model(model)
                net.prune_node_layerwise(test_data,1,len(n1),j,
                        do_bias_balance=True)
                net.prune_node_layerwise(test_data,2,len(n2),j,
                    do_bias_balance=True)
                net.SGD(training_data, retrain_epoch, 32, 0.5, lmbda=5.0,
                        evaluation_data=test_data,starting_layer = 1)
                lost,accuracy=net.model.evaluate(test_data[0],test_data[1])
                accu6.append(accuracy)
                K.clear_session()


        if do_without_bias:
            y1[j-1,:]=accu1
            y4[j-1,:]=accu4
        if do_without_bias:
            y2[j-1,:]=accu2
            y5[j-1,:]=accu5
        if do_retraining:
            y3[j-1,:]=accu3
            y6[j-1,:]=accu6

    x=np.arange(0,node_number)
    return y1,y2,y3,y4,y5,y6,x


def fresh_training(sizes, node_number, data = None, epoch = 50):
    """the sizes is the starting size, the network size is reduced
    during each iteration. By each iteration, the nodes number of all
    hidden layers are reduced by one
    """
    [training_data, validation_data, test_data] = data
    accu = []
    for i in range(node_number):
        reduced_size = sizes[:]
        for j in range(1,len(sizes)-1):
            reduced_size[j] = sizes[j] - i
        net = keras_network.Network(reduced_size)
        print(net.sizes)
        net.SGD(training_data, epoch, 32, 0.5, lmbda=5.0,
                evaluation_data=test_data)
        lost,accuracy=net.model.evaluate(test_data[0],test_data[1])
        accu.append(accuracy)
        K.clear_session()

    x=np.arange(0,node_number)
    return accu,x


def label_dependent_pruning(model, layer_index, node_number, data = None,
        choosen_quantity = 1, do_bias_balance = False):
    training_data, validation_data, test_data = data
    data=separator(test_data)
    net = keras_network.Network([])
    net.load_model(model)
    classes = net.sizes[-1]
    y=np.zeros((classes,node_number))
    for i in range(node_number):
        net.load_model(model)
        net.prune_node_layerwise(test_data,1,i,choosen_quantity, do_bias_balance=True)
        list_ = []
        for index in range(net.sizes[-1]):
            print('a')
            lost,accuracy=net.model.evaluate(data[index],
                          np.full((len(data[index]),1), index))
            list_.append(accuracy)
        K.clear_session()
        y[:,i]=list_
    return y


def models_prediction_comparison(data = None):
    training_data, validation_data, test_data = data
    net0 = keras_network.Network([])
    net1 = keras_network.Network([])
    net2 = keras_network.Network([])
    net3 = keras_network.Network([])
    net4 = keras_network.Network([])

    #load five trained models
    net0.load_model('784_100_100_10_1')
    net1.load_model('784_100_100_10_2')
    net2.load_model('784_100_100_10_3')
    net3.load_model('784_100_100_10_4')
    net4.load_model('784_100_100_10_5')

    #using these five models to predict the test data
    a0 = net0.model.predict(test_data[0])
    a1 = net1.model.predict(test_data[0])
    a2 = net2.model.predict(test_data[0])
    a3 = net3.model.predict(test_data[0])
    a4 = net4.model.predict(test_data[0])
    a0 = a0.argmax(axis=-1)
    a1 = a1.argmax(axis=-1)
    a2 = a2.argmax(axis=-1)
    a3 = a3.argmax(axis=-1)
    a4 = a4.argmax(axis=-1)

    diff_mat = np.zeros((5,5))
    for i in range(5):
        for j in range(5):
            diff_mat[i,j] = len(np.where(eval('a'+ (str)(i))!=eval('a'+ (str)(j)))[0])

    return diff_mat


def small_example(model, data = None, retrain_epoch = 20,
                      do_without_bias = True,
                      do_with_bias = True, do_retraining = False):

    [training_data, validation_data, test_data] = data
    net = keras_network.Network([])
    x = np.arange(0,90,3)
    accu_mi=[]
    accu_mi_w=[]
    accu_hy=[]
    accu_hy_w=[]
    accu_rd=[]
    accu_rd_w=[]
    accu_df=[]
    accu_df_s=[]
    for i in range(30):
        if do_without_bias:
            net.load_model(model)
            net.prune_node_layerwise(test_data,1,i,2)
            accu_hy.append(net.prune_node_layerwise(test_data,2,i*2,13))
            K.clear_session()


            net.load_model(model)
            net.prune_node_layerwise(test_data,1,i,2,
                    do_bias_balance=False)
            accu_mi.append(net.prune_node_layerwise(test_data,2,i*2,2,
                do_bias_balance=False))

            net.load_model(model)
            net.prune_node_layerwise(test_data,1,i,7,
                    do_bias_balance=False)
            accu_rd.append(net.prune_node_layerwise(test_data,2,i*2,7,
                do_bias_balance=False))

            K.clear_session()

        if do_with_bias:
            net.load_model(model)

            net.prune_node_layerwise(test_data,1,i,2,do_bias_balance = True)
            accu_hy_w.append(net.prune_node_layerwise(test_data,2,i*2,13,
                         do_bias_balance=True))
            K.clear_session()

            net.load_model(model)
            net.prune_node_layerwise(test_data,1,i,2,
                    do_bias_balance=True)
            accu_mi_w.append(net.prune_node_layerwise(test_data,2,i*2,2,
                do_bias_balance=True))

            net.load_model(model)
            net.prune_node_layerwise(test_data,1,i,7,
                    do_bias_balance=True)
            accu_rd_w.append(net.prune_node_layerwise(test_data,2,i*2,7,
                do_bias_balance=True))
            K.clear_session()

        if do_retraining:
            net.load_model(model)
            net.prune_node_layerwise(test_data,1,i,2)
            net.prune_node_layerwise(test_data,2,i*2,13,
                         do_bias_balance=True)
            net.SGD(training_data, retrain_epoch, 32, 0.5, lmbda=5.0,
                    evaluation_data=validation_data,starting_layer = 1)
            lost,accuracy=net.model.evaluate(test_data[0],test_data[1])
            accu3.append(accuracy)
            K.clear_session()


    net_data_free_surgery = keras_network.Network([])
    net_data_free_surgery.load_model(model)

    for i in range(30):
        if i ==0:
            accu_df_s.append(accu_mi[0])
        else:
            net_data_free_surgery.merge_node(1,1,surgery = True,
                    validation_data = validation_data)
            net_data_free_surgery.merge_node(2,2,surgery = True,
                    validation_data = validation_data)
            lost,accuracy=net_data_free_surgery.model.evaluate(test_data[0],test_data[1])
            accu_df_s.append(accuracy)

    K.clear_session()

    net_data_free = keras_network.Network([])
    net_data_free.load_model(model)
    for i in range(30):
        if i ==0:
            accu_df.append(accu_mi[0])
        else:
            net_data_free.merge_node(1,1,surgery = False,
                    validation_data = validation_data)
            net_data_free.merge_node(2,2,surgery = False,
                    validation_data = validation_data)
            lost,accuracy=net_data_free.model.evaluate(test_data[0],test_data[1])
            accu_df.append(accuracy)


    return accu_df,accu_hy,accu_mi,accu_rd,\
           accu_df_s,accu_hy_w,accu_mi_w,accu_rd_w,x

