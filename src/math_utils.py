import numpy as np
from math import log, inf



def quantize(obj, vRange = [0,1],quanConst = 2, sepaVec = None):
    if isinstance(obj, (int, float)) or obj.ndim == 0:
        return quantize_scalar(obj, vRange = vRange,quanConst = quanConst,
                sepaVec = sepaVec)
    elif obj.ndim == 1:
        return quantize_vec(obj, vRange = vRange,quanConst = quanConst,
                sepaVec = sepaVec)
    elif obj.ndim == 2:
        return quantize_mat(obj, vRange = vRange,quanConst = quanConst,
                sepaVec = sepaVec)
    else:
        raise InputError('the object can not be quantized')

def joint_distr(vec1, quanConst1, vec2, quanConst2):
    """ given two quantized vector with same size,
    return the joint pdf of two vector
    """
    zipped_vec = list(zip(vec1, vec2))
    j_distr = np.zeros((quanConst1, quanConst2))
    for i in range(quanConst1):
        for j in range(quanConst2):
            j_distr[i,j] = zipped_vec.count((i,j))
    return j_distr/ (len(vec1))

def joint2cond(joint_matrix):
    col_sums = joint_matrix.sum(axis=0)
    cond_matrix = joint_matrix / col_sums[np.newaxis, :]
    return cond_matrix

def joint2cond_entr(joint_matrix):
    cond_matrix = joint2cond(joint_matrix)
    marginal_p = joint2marginal(joint_matrix)
    entr = 0
    for i in range(joint_matrix.shape[1]):
        entr += marginal_p[i]*distr2entr(cond_matrix[:,i])
    return entr

def joint2marginal(joint_matrix, axis = 0):
    return np.sum(joint_matrix,axis = axis)

def joint2MI(joint_matrix):
    MI = 0
    for row, col, val in rolColVal(joint_matrix):
        MI = MI + val * log(val / (row * col), 2)
    return MI

def vec2distr(vec, quanConst):
    return np.bincount(vec, minlength = quanConst) / len(vec)

def distr2entr(distr):
    entr = 0
    for p in distr:
        if p > 0:
            entr = entr-p*log(p,2)
    return entr

def kl_divergence(P,Q):
    inf_cond = np.logical_and(P != 0, Q == 0)
    if np.any(inf_cond):
        return inf
    else:
        kld = 0
        for p,q in pqNotZero(zip(P,Q)):
            kld = kld + p * log(p/q, 2)
        return kld



#The following functions are called by the functions before
def quantize_scalar(val, vRange = [0,1],quanConst = 2, sepaVec = None):
    """ given a scalar and quantize it"""
    if sepaVec == None:
        step = (vRange[1] - vRange[0]) / quanConst
        quan_scalar = min(int((val - vRange[0]) // step), quanConst - 1)
    else:
        quan_scalar = quantize_scalar_sepa(val, sepaVec)
    return np.uint8(quan_scalar)

def quantize_vec(vec, vRange = [0,1],quanConst = 2, sepaVec = None):
    """ given a vector and quantize it"""
    quan_vec = np.zeros(len(vec),dtype = np.uint8)
    if sepaVec == None:
        step = (vRange[1] - vRange[0]) / quanConst
        for idx,val in enumerate(vec):
            quan_vec[idx] = min(int((val - vRange[0]) // step), quanConst - 1)
    else:
        for idx,val in enumerate(vec):
            quan_vec[idx] = quantize_scalar_sepa(val, sepaVec)
    return quan_vec

def quantize_mat(mat, vRange = [0,1],quanConst = 2, sepaVec = None):
    """ given a vector and quantize it"""
    quan_mat = np.zeros(mat.shape,dtype = np.uint8)
    if sepaVec == None:
        step = (vRange[1] - vRange[0]) / quanConst
        for i, vec in enumerate(mat):
            for idx,val in enumerate(vec):
                quan_mat[i, idx] = min(int((val - vRange[0]) // step), quanConst - 1)
    else:
        for i, vec in enumerate(mat):
            for idx,val in enumerate(vec):
                quan_mat[i, idx] = quantize_scalar_sepa(val, sepaVec)
    return quan_mat

def quantize_scalar_sepa(val, sepaVec):
    """ quantize a scalar value based on
    the bound given by sepaVec
    """
    for idx, bound in enumerate(sepaVec):
        if not val > bound:
            return idx
    return idx + 1

def pqNotZero(stream):
    """given a zipped (p,q) iterator, return the terms
    where both q and p is none zeros.
    """
    for p,q in stream:
        if q !=0 and p !=0:
            yield p,q

def range2sepa(vRange, quanConst):
    step = (vRange[1] - vRange[0]) / quanConst
    return [x*step for x in range(1,quanConst)]

def rolColVal(stream):
    for i in range(stream.shape[0]):
        for j in range(stream.shape[1]):
            if not stream[i,j] ==0:
                yield np.sum(stream[i,:]), np.sum(stream[:,j]), stream[i,j]

"""

from math_utils import *
import numpy as np
a1 = [0,0.1,0.2,0.2,0.3,0.5,0.7,1,1,0.8]
a2 = [0,0.0,0.5,0.7,0.7,0.2,0.7,0,0,0.8]
a3 = [0,2,1,2,0,3,3,1,0,0]
a1 = np.array(a1)
a2 = np.array(a2)
a3 = np.array(a3)
b1 = quantize_vec(a1)
b2 = quantize_vec(a2)
c1 = vec2distr(b1, 2)
J = joint_distr(b1,2,a3,4)
C = joint2cond(J)
a = [[1,2],[3,4],[5,6]]
a = np.array(a)
b = a/10
row_sums = a.sum(axis=1)
new_matrix = a / row_sums[:, np.newaxis]
distr2MI(b)



"""
