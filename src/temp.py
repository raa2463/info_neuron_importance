        self.sizes[layer_idx]= self.sizes[layer_idx]-len(node_list)
        model_ = Sequential()
        for i in range(len(self.sizes)-1):
            if i == len(self.sizes)-2: #this is the output layer
                actv_str = self.out_actv
            else:  #this is the hidden layer
                actv_str = self.hid_actv

            if i == layer_idx-1:
                temp_weight1,temp_biases1=self.model.layers[i].get_weights()
                temp_weight1 = np.delete(temp_weight1, node_list, axis=1)
                temp_biases1=np.delete(temp_biases1,node_list,axis=0)
                model_.add(Dense(self.sizes[i+1], activation=actv_str,
                    input_dim=self.sizes[i],weights=[temp_weight1,temp_biases1]))
            elif i == layer_idx:
                temp_weight2,temp_biases2=self.model.layers[i].get_weights()
                temp_weight2 = np.delete(temp_weight2, node_list, axis=0)
                model_.add(Dense(self.sizes[i+1], activation=actv_str,
                    input_dim=self.sizes[i],weights=[temp_weight2,temp_biases2]))
            else:
                model_.add(Dense(self.sizes[i+1], activation=actv_str,
                    input_dim=self.sizes[i],
                    weights=self.model.layers[i].get_weights()))

        model_.compile(optimizer=optimizers.Adadelta(),
                loss='sparse_categorical_crossentropy',metrics=['accuracy'])
        self.model = model_


