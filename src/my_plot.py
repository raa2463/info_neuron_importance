import numpy as np
import matplotlib.pyplot as plt
from matplotlib import ticker
from matplotlib.font_manager import FontProperties
import matplotlib.colors as colors_
import keras_network
from matplotlib.ticker import MaxNLocator


#color type and linestyle for plt.plot
linestyles = ['_', '-', '--', ':', '-.']
colors = ['xkcd:teal', 'xkcd:green', 'xkcd:blue',
          'xkcd:pink', 'xkcd:brown', 'xkcd:red',
          'xkcd:light blue', 'xkcd:purple', 'xkcd:orange',
          'xkcd:light green', 'xkcd:magenta', 'xkcd:yellow',
          'xkcd:dark green', 'xkcd:red', 'xkcd:salmon',
          'xkcd:beige', 'xkcd:mustard', 'xkcd:red',
          'xkcd:beige', 'xkcd:mustard', 'xkcd:indigo']

colors__ = ['xkcd:teal', 'xkcd:green', 'xkcd:blue',
          'xkcd:pink', 'xkcd:brown', 'xkcd:red',
          'xkcd:light blue', 'xkcd:purple', 'xkcd:orange',
          'xkcd:light green', 'xkcd:magenta', 'xkcd:yellow',
          'xkcd:dark green', 'xkcd:dark blue', 'xkcd:salmon',
          'xkcd:beige', 'xkcd:mustard', 'xkcd:indigo',
          'xkcd:beige', 'xkcd:mustard', 'xkcd:indigo']


labels = ['entropy','mutual information','max entropy',
          'fake mutual information', 'labeled mutual information',
          'output norm','random','max kl divergence',
          'negated labeled min entropy', 'labeled max kl divergence',
          'Jenson Shannon Divergence', 'naive JS divergence',
          'KL selectity','quantity14',
          'quantity15','quantity16',
          'quantity17','quantity18',
          'data-free without surgery','data-free with surgery',
          ]


def plot_matrix(cell_data, fig, ax, absolute=False):
    """using colormap to plot a matrix,
    the function takes three inputs, the matrix,
    fig, and ax. An optional parameter absolute is
    set to False in default. When set to true,
    the absolute value of the weight matrix wil
    be taken.
    """
    if absolute:
        cell_data = np.abs(cell_data)
    x=len(cell_data)
    y=len(cell_data[0])
    cax = ax.imshow(cell_data, aspect='auto', cmap='magma', interpolation='none',
                    extent=[0,y,0,x], origin='lower')
    cb= fig.colorbar(cax)
    tick_locator = ticker.MaxNLocator(nbins=5)
    xa = ax.get_xaxis()
    xa.set_major_locator(plt.MaxNLocator(integer=True))
    ya = ax.get_yaxis()
    ya.set_major_locator(plt.MaxNLocator(integer=True))
    cb.locator = tick_locator
    cb.update_ticks()


def plot_1D_distribution(data, fig, ax, do_autolabel = False):
    """1D distribution of data will be plot,
    a 1D array is expected. The second and the third
    parameters are fig and ax, there is a optional parameter
    called do_autolabel, when set to true, the value of the vector
    will be labeled above the corresponding bars.
    """
    N = len(data)
    ind = np.linspace(0,1,N+1)  # the x locations for the groups
    width = 1.0/N     # the width of the bars
    rects1 = ax.bar(ind[:-1] + width/2, data, width, color='b',edgecolor='c')
    # add some text for labels, title and axes ticks
    ax.set_ylabel('probability')
    ax.set_xlabel('node output')
    ax.set_xticks([0,0.2,0.4,0.6,0.8,1])

    def autolabel(rects):
        """
        Attach a text label above each bar displaying its height
        """
        for rect in rects:
            height = rect.get_height()
            ax.text(rect.get_x() + rect.get_width()/2., height,
                    '%1.2f' % height,fontsize=7,
                    ha='center', va='bottom')
    if do_autolabel:
        autolabel(rects1)
    plt.grid()


def subplot_pruning_curve(data, fig, ax, quantity_list):
    plt.subplot(2, 2, 1)
    plt.plot(x, y1[0,:], 'b-', label='$H(i)$')
    plt.subplot(2, 2, 1)
    plt.plot(x, y1[1,:], 'g-', label='$I(i;Y)$')
    plt.subplot(2, 2, 1)
    plt.plot(x, y1[2,:], 'r-', label='$H(i)-min H(i|j)$')
    plt.subplot(2, 2, 1)
    plt.plot(x, y1[5,:], 'c-', label='output norm')
    plt.subplot(2, 2, 1)
    plt.plot(x, y1[6,:], 'm-', label='random group')
    plt.ylim((0.5,1))
    plt.title('accuracy node dropping plot for 4 different criterion without bias balancing')
    plt.ylabel('accuracy')
    plt.legend(loc='best')
    plt.grid(True)

    plt.subplot(2, 2, 2)
    plt.plot(x, y2[0,:], 'b-.', label='$H(i)$ with bias balancing')
    plt.subplot(2, 2, 2)
    plt.plot(x, y2[1,:], 'g-.', label='$I(i;Y)$ with bias balancing')
    plt.subplot(2, 2, 2)
    plt.plot(x, y2[2,:], 'r-.', label='$H(i)-min H(i|j)$ with bias balancing')
    plt.subplot(2, 2, 2)
    plt.plot(x, y2[5,:], 'c-', label='output norm with bias balancing')
    plt.subplot(2, 2, 2)
    plt.plot(x, y2[6,:], 'm-', label='random group with bias balancing')
    plt.ylim((0.5,1))
    plt.title('accuracy node dropping plot for 4 different criterion with bias balancing')
    plt.ylabel('accuracy')
    plt.legend(loc='best')
    plt.grid(True)

    plt.subplot(2, 2, 3)
    plt.plot(x, y3[0,:], 'b-.', label='$H(i)$ with retraining')
    plt.subplot(2, 2, 3)
    plt.plot(x, y3[1,:], 'g-.', label='$I(i;Y)$ with retraining')
    plt.subplot(2, 2, 3)
    plt.plot(x, y3[2,:], 'r-.', label='$H(i)-min H(i|j)$ with retraining')
    plt.subplot(2, 2, 3)
    plt.plot(x, y3[5,:], 'c-', label='output norm with retraining')
    plt.subplot(2, 2, 3)
    plt.plot(x, y3[6,:], 'm-', label='random group with retraining')
    plt.ylim((0.5,1))
    plt.title('accuracy node dropping plot for 4 different criterion with retraining')
    plt.ylabel('accuracy')
    plt.legend(loc='best')
    plt.grid(True)


    plt.subplot(2, 2, 4)
    plt.plot(x, y1[0,:], 'b-.', label='$H(i)$')
    plt.subplot(2, 2, 4)
    plt.plot(x, y2[0,:], 'g-.', label='$H(i)$ with bias balancing')
    plt.subplot(2, 2, 4)
    plt.plot(x, y3[0,:], 'r-.', label='$H(i)$ with retraining')
    '''plt.subplot(2, 2, 1)
    plt.plot(x, y2[3,:], 'p-', label='random group')'''
    plt.ylim((0.5,1))
    plt.title('accuracy node dropping plot for the same criterion')
    plt.ylabel('accuracy')
    plt.legend(loc='best')
    plt.grid(True)


def plot_pruning_curve(data, fig, ax, do_with_bias = True,
        do_without_bias = True, do_data_free = False,
        do_retrain = True, title = None, quantity_list=[1,7],
        ylim=(0.3,1)):
    if len(data)==3:
        y1, y2, x = data
        do_y3 = False
    elif len(data)==4:
        y1, y2, y3, x = data
        do_y3 = True

    if do_with_bias==True and do_without_bias==False:
        tile_label = 0
    else:
        tile_label = 1
    data_free_without_bias=False
    data_free_with_bias=False
    if 19 in quantity_list:
        data_free_without_bias=True
        quantity_list.remove(19)

    if 20 in quantity_list:
        data_free_with_bias=True
        quantity_list.remove(20)

    LS_idx = 1
    if do_with_bias:
        for quantity in quantity_list:
            plt.plot(x, y2[quantity - 1, :], linestyles[LS_idx],
                     color = colors[quantity], label = labels[quantity - 1] +
                     tile_label*' with bias')
        LS_idx += 1

    if do_without_bias:
        for quantity in quantity_list:
            plt.plot(x, y1[quantity - 1, :], linestyles[LS_idx],
                     color = colors[quantity], label = labels[quantity - 1])
        LS_idx += 1

    if do_y3 & do_retrain:
        for quantity in quantity_list:
            plt.plot(x, y3[quantity - 1, :], linestyles[LS_idx],
                     color = colors[quantity], label = labels[quantity - 1] +
                     ' with retraining')
        LS_idx += 1

    if do_y3 & do_data_free:
        plt.plot(x, y3[0,:], linestyles[LS_idx],color = color[1], label='without surgery')
        plt.plot(x, y3[1,:], linestyles[LS_idx],color = color[2], label='with surgery')

    LS_idx+=1
    if data_free_without_bias:
        plt.plot(x, y1[18,:], linestyles[1],color = colors[10], label='without surgery')
    if data_free_with_bias:
        plt.plot(x, y1[19,:], linestyles[2],color = colors[10], label='with surgery')

    plt.ylim(ylim)
    plt.ylabel('accuracy')
    plt.xlabel('number of pruned nodes')
    if title:
        plt.title(title)
    xa = ax.get_xaxis()
    xa.set_major_locator(plt.MaxNLocator(integer=True))
    plt.legend(loc='best')
    plt.grid(True)


def plot_accu_mat(cell_data,fig,ax):
    classes = len(cell_data)
    node_number = len(cell_data[0])
    xa = ax.get_xaxis()
    xa.set_major_locator(plt.MaxNLocator(integer=True))
    ya = ax.get_yaxis()
    ya.set_major_locator(plt.MaxNLocator(integer=True))
    ax.set_ylabel('classes')
    ax.set_xlabel('# of nodes pruned')
    cax = ax.imshow(cell_data, aspect='auto', cmap='magma', interpolation='none',
                    extent=[0,node_number,0,classes], origin='lower', vmin=0.5, vmax=1)
    cb = fig.colorbar(cax)
    cb.set_label('probability', rotation=270)


def plot_node_rank(model, fig, ax, reranking = False, show_value = False,
        rerank_quantity = 0, layer_index = 1,
        enable_color = False, quantity_list = [1,2,3,4]):
    net = keras_network.Network([])
    net.load_model(model)
    node_number = net.sizes[layer_index]
    layer_index = layer_index
    cell_data = net.model.layers[layer_index].get_weights()[0]
    cell_data = np.abs(cell_data)
    x=len(net.model.layers[layer_index].get_weights()[0])
    y=len(net.model.layers[layer_index].get_weights()[0][0])

    #get correspondending quantity
    data = np.zeros((len(quantity_list),node_number))
    rank = np.zeros((len(quantity_list),node_number))
    for idx, quantity_idx in enumerate(quantity_list):
        data[idx,:] = net.quantity_idx_dict[quantity_idx][layer_index][:node_number]
        rank[idx,:] = np.argsort(np.argsort(data[idx,:]))+1
    if show_value:
        cell=np.flipud(np.transpose(data))
    else:
        cell=np.flipud(np.transpose(rank))


    if reranking:
        cell_data = np.flipud(cell_data)
        cell_data = cell_data[cell[:,rerank_quantity].argsort()]

    cax = ax.imshow(cell_data, aspect='auto', cmap='magma',
                    interpolation='none',extent=[0,y,0,x], origin='lower')

    cb= fig.colorbar(cax)
    tick_locator = ticker.MaxNLocator(nbins=5)
    cb.locator = tick_locator
    cb.update_ticks()


    if reranking:
        cell = np.flipud(cell[cell[:,rerank_quantity].argsort()])#reorder the rank of nodes

    cell_text=[]
    for row in range(node_number):
        apd=[]
        for x in cell[row]:
            if x>node_number:
                apd.append("")
            else:
                if show_value:
                    apd.append('%1.4f' % (x))
                else:
                    apd.append('%1.0f' % (x))
        cell_text.append(apd)

    columns = tuple(str(quantity_idx) for quantity_idx in quantity_list)
    rows=np.flipud(np.arange(node_number))
    colors = plt.cm.BuPu(np.linspace(0, 0.5, len(rows)))
    colors = colors[::-1]


    if reranking:
        rows = rows[cell[:,rerank_quantity].argsort()]#reorder the label

    if show_value:
        pass
    else:
        mapping = [[(float(x)-1)/60 for x in y] for y in cell_text]

    if enable_color:
        the_table = plt.table(cellText=cell_text,
                              #rowLabels=rows,
                              #rowColours=colors,
                              colLabels=columns,loc='left',
                              cellColours=plt.cm.BuPu(mapping),
                              bbox=[-0.2, 0, 0.2, 1.0333333])
    else:
        the_table = plt.table(cellText=cell_text,
                              #rowLabels=rows,
                              #rowColours=colors,
                              colLabels=columns,loc='left',
                              bbox=[-0.2, 0, 0.2, 1.0333333])


    the_table.auto_set_font_size(False)
    the_table.set_fontsize(6)
    the_table.scale(1,1)

    plt.yticks([])
    plt.subplots_adjust(left=0.2)
    return cell_text


def quantities_comparison(model, fig, ax):
    net = keras_network.Network([])
    net.load_model(model)

    data = np.zeros((3,30))
    sort_index = np.zeros((3,30))
    data[0,:]=net.entropy[1][:30]
    data[1,:]=net.mutual_information[1][:30]
    data[2,:]=net.max_entropy[1][:30]
    sort_index[0,:]=np.argsort(np.argsort(data[0,:]))+1
    sort_index[1,:]=np.argsort(np.argsort(data[1,:]))+1
    sort_index[2,:]=np.argsort(np.argsort(data[2,:]))+1
    x=np.arange(0,30)

    #columns = ['node %d' % xx for xx in np.arange(30)]
    #rows =  ('entropy', 'mutual mutual_information', 'max entropy')


    plt.scatter(x,data[0,:],color='red',s=75, label='entropy')
    plt.scatter(x,data[1,:],color='blue',s=75, label='mutual information')
    plt.scatter(x,data[2,:],color='green',s=75, label='max entropy')
    for i, txt in enumerate(sort_index[0,:]):
        ax.annotate(int(txt), (x[i]+0.1,data[0,i]), color='darkred')

    for i, txt in enumerate(sort_index[1,:]):
        ax.annotate(int(txt), (x[i]+0.1,data[1,i]), color='darkblue')

    for i, txt in enumerate(sort_index[2,:]):
        ax.annotate(int(txt), (x[i]+0.1,data[2,i]), color='darkgreen')

    #minor_ticks = np.arange(0, 30, 1)
    #ax.set_xticks(minor_ticks)
    ax.set_ylabel('corresponding quantity')
    ax.set_xlabel('node index')
    plt.subplots_adjust(left=0.2, bottom=0.2)
    plt.xlim((-1,30))
    #plt.xticks(fontsize=20)
    #plt.yticks(fontsize=20)
    ax.legend(loc='best', framealpha = 0.5)


def plot_quantity_distribution(list1, list2, quantity_idx, fig, ax):
    node_number = len(list1)
    all_one = np.ones(node_number)
    node_number = len(list2)
    all_two = np.ones(node_number)*2
    violin_parts = ax.violinplot([list1,list2], [1,2], points=20, widths=0.3,
        showmeans=True, showextrema=True, showmedians=True)
    #plt.scatter(all_one,list1,color='red',s=75, label=labels[quantity_idx-1]+' in layer 1')
    #plt.scatter(all_two,list2,color='blue',s=75, label=labels[quantity_idx-1]+' in layer 2')
    #for pc in violin_parts['bodies']:
    #    pc.set_facecolor('red')
    #    pc.set_edgecolor('black')

    ax.set_xlabel('layer')
    ax.set_ylabel('quantity value')
    plt.xlim((0.6,2.4))
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))


def plot_evenly_pruning_curve(data, fig, ax):
    y1, y2, y3, y4, y5, y6, x = data
    x = x * 2
#    plt.plot(x, y2[1,:], linestyles[1],color = colors[2], label='mutual information evenly')
    plt.plot(x, y5[1,:], linestyles[2],color = colors[2], label='mutual information jointly')
#    plt.plot(x, y2[12,:], linestyles[1],color = colors[3], label='KL selectivity evenly')
    plt.plot(x, y5[12,:], linestyles[2],color = colors[3], label='KL selectivity jointly')
    plt.plot(x, y2[6,:], linestyles[1],color = colors[4], label='random')
#    plt.plot(x, y2[0,:], linestyles[1],color = colors[5], label='entropy evenly')
    plt.plot(x, y5[0,:], linestyles[2],color = colors[5], label='entropy jointly')

    plt.ylim((0.3,1))
    plt.ylabel('accuracy')
    plt.xlabel('number of pruned nodes')
    xa = ax.get_xaxis()
    xa.set_major_locator(plt.MaxNLocator(integer=True))
    plt.legend(loc='best')
    plt.grid(True)


def pruning_distribution(distri_data,fig,ax):
    N = 20
    ind = np.linspace(0,1,N+1)  # the x locations for the groups
    width = 1.0/N     # the width of the bars
    rects1 = ax.bar(ind[:-1] + width/2, distri_data, width, color='b',edgecolor='c')
    # add some text for labels, title and axes ticks
    ax.set_ylabel('probability')
    ax.set_title('Distribution of Node Output')
    ax.set_xticks([0,0.2,0.4,0.6,0.8,1])
    plt.grid(True)

    def autolabel(rects):
        """
        Attach a text label above each bar displaying its height
        """
        for rect in rects:
            height = rect.get_height()
            ax.text(rect.get_x() + rect.get_width()/2., height,
                    '%1.2f' % height,fontsize=7,
                    ha='center', va='bottom')

    autolabel(rects1)


def plot_small_example(fig, ax, data):
    [accu_df,accu_hy,accu_mi,accu_rd,accu_df_s,accu_hy_w,accu_mi_w,accu_rd_w,x] = data
    plt.plot(x, accu_hy, linestyles[1],color = colors[1], label='hybrid without bias')
    plt.plot(x, accu_hy_w, linestyles[2],color = colors[1], label='hybrid with bias')
    plt.plot(x, accu_mi, linestyles[1],color = colors[2], label='jointly mutual information without bias')
    plt.plot(x, accu_mi_w, linestyles[2],color = colors[2], label='jointly mutual information with bias')
    plt.plot(x, accu_rd, linestyles[1],color = colors[3], label='random')
    plt.plot(x, accu_rd_w, linestyles[2],color = colors[3], label='random with bias')
    plt.plot(x, accu_df, linestyles[1],color = colors[4], label='data-free')
    plt.plot(x, accu_df_s, linestyles[2],color = colors[4], label='data-free with surgery')

    plt.ylim((0.3,1))
    plt.ylabel('accuracy')
    plt.xlabel('number of pruned nodes')
    xa = ax.get_xaxis()
    xa.set_major_locator(plt.MaxNLocator(integer=True))
    plt.legend(loc='best')
    plt.grid(True)


def plot_quantization_curve(data, fig, ax):
    y1_2,y2_2,y3_2,y1_4,y2_4,y3_4,y1_8,y2_8,y3_8,y1_16,y2_16,y3_16,y1_32,y2_32,y3_32,x = data
    plt.plot(x, y2_2[1], linestyles[1],color = colors[1], label='mutual information 1 bit quantization')
    plt.plot(x, y2_4[1], linestyles[1],color = colors[2], label='mutual information 2 bit quantization')
    plt.plot(x, y2_8[1], linestyles[1],color = colors[3], label='mutual information 3 bit quantization')
    plt.plot(x, y2_16[1], linestyles[1],color = colors[4], label='mutual information 4 bit quantization')
    plt.plot(x, y2_32[1], linestyles[1],color = colors[5], label='mutual information 5 bit quantization')

    plt.ylim((0.3,1))
    plt.ylabel('accuracy')
    plt.xlabel('number of pruned nodes')
    xa = ax.get_xaxis()
    xa.set_major_locator(plt.MaxNLocator(integer=True))
    plt.legend(loc='best')
    plt.grid(True)


def plot_inverse_curve(data,data2, fig, ax, do_with_bias = True,
        do_without_bias = True, do_data_free = False,
        do_retrain = True, title = None, quantity_list=[1,7],
        ylim=(0.3,1)):
    y1, y2, y3, x = data
    y1_, y2_, y3_, x = data2


    LS_idx = 1
    for quantity in [1,2,7,13]:
        plt.plot(x, y2[quantity - 1, :], linestyles[LS_idx],
                 color = colors[quantity], label = labels[quantity - 1]
                 + ' low value pruning')
    LS_idx = 3

    for quantity in [1,2,13]:
        plt.plot(x, y2_[quantity - 1, :], linestyles[LS_idx],
                 color = colors[quantity], label = labels[quantity - 1]
                 + ' high value pruning')

    plt.ylim(ylim)
    plt.ylabel('accuracy')
    plt.xlabel('number of pruned nodes')
    if title:
        plt.title(title)
    xa = ax.get_xaxis()
    xa.set_major_locator(plt.MaxNLocator(integer=True))
    plt.legend(loc='best')
    plt.grid(True)








