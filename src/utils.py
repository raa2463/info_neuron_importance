import numpy as np
from IPython.display import HTML, display

def separator(data):
    '''a=[[[1,2,3],[2,3,4],[3,4,5],[4,5,6],[5,6,7]],[0,1,3,2,3]]
    separator(a)
    lost,accuracy=net.model.evaluate(test_data[0],test_data[1])
    '''
    class_number = max(data[1])+1
    zipped = list(zip(data[1],range(len(data[1]))))
    sorted_data = []
    for index in np.arange(class_number):
        list_ = [index_ for (value,index_) in zipped if value==index]
        sorted_data.append(np.array([data[0][x] for x in list_]))
    return np.array(sorted_data)


def plot_table(data):
    display(HTML(
        '<table><tr>{}</tr></table>'.format(
            '</tr><tr>'.join(
                '<td>{}</td>'.format('</td><td>'.join(str(_) for _ in row)) for row in data)
    )))


def data_info(data):
    result = np.zeros((len(data)),dtype = bool)
    for i in range(len(data)):
        result[i] = True if data[i][0] else False
    return result


def max_gen(list1,list2,number):
    len1=len(list1)
    list_=np.concatenate((list1,list2))	
    max_=np.argsort(list_)[:number]
    return1 = max_[max_<len1]
    return2 = max_[max_>=len1]-len1
    return return1,return2
