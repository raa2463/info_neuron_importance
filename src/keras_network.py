#force to run the tensorflow cpu version
import os
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = ""

import sys
from keras.models import Sequential
from keras.layers import Dense, Activation
from keras import regularizers, optimizers
from keras.callbacks import LambdaCallback
from keras.models import load_model
import h5py
import numpy as np
from math import log
from itertools import product
from numpy import linalg as LA
from cythonized.math_utils import *
import my_plot
#from math_utils import * #uncomment to use the python implementation


class Network(object):

    def __init__(self, sizes, quan_const = 2, sepaVec = None,
                 hid_actv = 'sigmoid', out_actv = 'softmax'):
        self.sizes = sizes
        self.sepaVec = sepaVec
        self.quan_const = quan_const
        self.hid_actv = hid_actv
        self.out_actv = out_actv
        self.num_layers = len(sizes)

        self.model = Sequential()
        if sizes:
            for i in range(len(self.sizes)-2):
                self.model.add(Dense(self.sizes[i+1], activation=self.hid_actv,
                                     input_dim=self.sizes[i]))
            self.model.add(Dense(self.sizes[i+2], activation = self.out_actv,
                input_dim=self.sizes[i+1]))

        self.entropy = [np.zeros(y) for y in self.sizes[:]]
        self.cond_entropy = [np.zeros((y, self.sizes[-1])) for y in self.sizes[:]]
        self.distribution = [np.zeros((y,self.quan_const)) for y in self.sizes[:]]
        self.distribution_mean = [np.zeros(y) for y in self.sizes[:]]
        self.cond_distribution = [np.zeros((y, self.sizes[-1],self.quan_const))
                                  for y in self.sizes[:]]
        self.lr=0.0001

    def SGD(self, training_data, epochs, mini_batch_size,
            lr, evaluation_data=None, lmbda = 0.01,
            starting_layer = 0, adam = False):

        """"basic setting for learning"""
        self.lr=lr
        if adam == False:
            self.sgd = optimizers.SGD(lr=lr, decay=0.0, momentum=0.0, nesterov=False)
        else:
            self.sgd = optimizers.Adam(lr=lr, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
        model_ = Sequential()
        for i in range(len(self.sizes)-2):
            model_.add(Dense(self.sizes[i+1], activation = self.hid_actv,
                input_dim=self.sizes[i],
                weights=self.model.layers[i].get_weights()))
        model_.add(Dense(self.sizes[i+2], activation = self.out_actv,
            input_dim=self.sizes[i+1],
            weights=self.model.layers[i+1].get_weights()))

        """set regularizer for weight matrix in each layer"""
        for i in range(len(self.sizes)-1):
            model_.layers[i].kernel_regularizer=regularizers.l2(lmbda)

        """freeze the input layer so the weight matrix doesn't change"""
        for i in range(starting_layer):
            model_.layers[i].trainable = False
        model_.compile(optimizer=self.sgd,
            loss='sparse_categorical_crossentropy',
            metrics=['accuracy'])
        self.model=model_

        """rewrite the status bar when training, using callback function"""
        batch_print_callback = LambdaCallback(
            on_epoch_end=lambda epoch, logs: status_bar(epoch+1,epochs,extra_log =logs['acc'] ))

        """finally do the training"""
        self.model.fit(training_data[0], training_data[1],
            epochs=epochs, batch_size=mini_batch_size, verbose = 0,
            callbacks = [batch_print_callback],
            validation_data=(evaluation_data[0], evaluation_data[1]))

    def calculate_entropy(self,training_data):
        self.data_distribution =  [len(np.where(training_data[1]==i)[0]) \
                / float(len(training_data[1])) for i in range(self.sizes[-1])]
        predicted_label = self.model.predict_classes(training_data[0])
        self.pred_data_distr =  [len(np.where(predicted_label==i)[0])/float(len(predicted_label))
                for i in range(self.sizes[-1])]
        self.distribution = [np.zeros((y,self.quan_const)) for y in self.sizes[:]]
        self.cond_distribution = [np.zeros((y, self.sizes[-1],self.quan_const)) \
                for y in self.sizes[:]]
        self.labeled_mutual_information = [np.zeros((y, self.sizes[-1])) for y in self.sizes[:]]
        self.labeled_kl_diverge = [np.zeros((y, self.sizes[-1])) for y in self.sizes[:]]
        self.labeled_entropy = [np.zeros((y, self.sizes[-1])) for y in self.sizes[:]]
        self.pred_cond_distr = [np.zeros((y, self.sizes[-1],self.quan_const)) \
                for y in self.sizes[:]]

        for layer_idx in range(1,len(self.sizes)-1):
            print("calculating the entropy of hidden layer {} ...".format(layer_idx))

            for node_idx in range(self.sizes[layer_idx]):
                self.distribution[layer_idx][node_idx,:] \
                    =vec2distr(self.quantized_actv[layer_idx][:,node_idx],
                               quanConst = self.quan_const)

                self.entropy[layer_idx][node_idx] \
                    =distr2entr(self.distribution[layer_idx][node_idx,:])

                self.distribution_mean[layer_idx][node_idx] \
                    =np.mean(self.activations[layer_idx][:,node_idx])

                for label_idx in range(self.sizes[-1]):
                    self.cond_entropy[layer_idx][node_idx,label_idx] = distr2entr(
                        vec2distr(
                        self.quantized_actv[layer_idx][
                        training_data[1]==label_idx, node_idx],
                        quanConst = self.quan_const))


                    self.cond_distribution[layer_idx][node_idx,label_idx,:] \
                        =vec2distr(self.quantized_actv[layer_idx][
                            training_data[1] ==label_idx,node_idx],
                               quanConst = self.quan_const)

                    self.pred_cond_distr[layer_idx][node_idx,label_idx] \
                        =vec2distr(self.quantized_actv[layer_idx][
                            predicted_label==label_idx,node_idx],
                               quanConst = self.quan_const)

                    joint = joint_distr( #get the joint distribution of node output and labeled Y
                                self.quantized_actv[layer_idx][:, node_idx],
                                self.quan_const,
                                np.array(training_data[1]==label_idx,dtype = int), 2,
                                )

                    cond = joint2cond(joint)

                    self.labeled_mutual_information[layer_idx][node_idx,label_idx] \
                            = joint2MI(joint)

                    self.labeled_entropy[layer_idx][node_idx,label_idx] \
                        = joint2cond_entr(np.transpose(joint))

                    self.labeled_kl_diverge[layer_idx][node_idx,label_idx] \
                        = kl_divergence(cond[:,0],cond[:,1])

        self.calculate_kl_divergence(training_data)
        self.calculate_JS_divergence(training_data)
        self.calculate_KL_cond_joint_divergence(training_data)

    def calculate_information_quantity(self):
        self.mutual_information = [np.zeros(y) for y in self.sizes[:]]
        self.max_entropy        = [np.zeros(y) for y in self.sizes[:]]
        self.quantity4          = [np.zeros(y) for y in self.sizes[:]]
        self.max_labeled_mutual_information = [np.zeros(y) for y in self.sizes[:]]
        self.output_norm        = [np.zeros(y) for y in self.sizes[:]]
        self.max_kl_diverge     = [np.zeros(y) for y in self.sizes[:]]
        self.labeled_min_entropy= [np.zeros(y) for y in self.sizes[:]]
        self.max_labeled_kl_div = [np.zeros(y) for y in self.sizes[:]]
        if not hasattr(self, 'random_group'):
            self.random_group       = [np.random.randn(y) for y in self.sizes[:]]

        for layer_idx in range(1,self.num_layers-1):
            """looping over all the hidden nodes"""
            tmp_list=[]
            print("calculating the quantities on hidden layer {} ".format(layer_idx))
            for node_idx in range(self.sizes[layer_idx]):
                self.mutual_information[layer_idx][node_idx] \
                    = self.entropy[layer_idx][node_idx] \
                    - np.inner(self.data_distribution,
                               self.cond_entropy[layer_idx][node_idx,:])

                self.quantity4[layer_idx][node_idx] \
                    = self.entropy[layer_idx][node_idx] \
                        - np.inner(self.pred_data_distr,
                                   self.cond_entropy[layer_idx][node_idx,:])

                self.max_entropy[layer_idx][node_idx] \
                    = self.entropy[layer_idx][node_idx] \
                    - np.amin(self.cond_entropy[layer_idx][node_idx,:])

                self.max_labeled_mutual_information[layer_idx][node_idx] \
                    = np.amax(self.labeled_mutual_information[layer_idx][node_idx, :])

                self.labeled_min_entropy[layer_idx][node_idx] \
                    = -np.amin(self.labeled_entropy[layer_idx][node_idx, :]) #value is negated

                self.max_labeled_kl_div[layer_idx][node_idx] \
                    = np.amax(self.labeled_kl_diverge[layer_idx][node_idx, :])

                self.output_norm[layer_idx][node_idx] \
                    = LA.norm(self.model.layers[layer_idx].get_weights()[0][node_idx,:],1) \
                    /(self.sizes[layer_idx+1])
                candidate=0

                current_max = 0
                for i in range(self.sizes[-1]):
                    for j in range(i+1, self.sizes[-1]):
                        temp_quan = min(self.kl_diverge[layer_idx][node_idx,i,j], \
                                self.kl_diverge[layer_idx][node_idx,j,i])
                        if temp_quan > current_max:
                            current_max = temp_quan
                self.max_kl_diverge[layer_idx][node_idx] = current_max

    def prune_node_layerwise(self,accuracy_data,layer_idx,number_nodes_pruned,
                             quantity_choice,do_bias_balance = False,
                             inversed_quantity=False):
        self.quantity_idx_dict = {1 : self.entropy,
                             2 : self.mutual_information,
                             3 : self.max_entropy,
                             4 : self.quantity4,
                             5 : self.max_labeled_mutual_information,
                             6 : self.output_norm,
                             7 : self.random_group,
                             8 : self.max_kl_diverge,
                             9 : self.labeled_min_entropy,
                             10: self.max_labeled_kl_div,
                             11: self.max_JS_diverge,
                             12: self.naive_max_JS_diverge,
                             13: self.max_cond_joint_kl_div,
                             }
        quantity = self.quantity_idx_dict[quantity_choice]
        if inversed_quantity and number_nodes_pruned:
            delete_list = np.argsort(quantity[layer_idx][:])\
                    [-number_nodes_pruned:]
        else:
            delete_list = np.argsort(quantity[layer_idx][:])[:number_nodes_pruned]
        if do_bias_balance:
            self.delete_node_with_bias(layer_idx,delete_list)
        else:
            self.delete_node(layer_idx,delete_list)
        print("current size is:{}, with quantity {}".format(
              self.sizes,my_plot.labels[quantity_choice-1]))
        lost,accuracy=self.model.evaluate(accuracy_data[0],accuracy_data[1])
        print("Accuracy on evaluation data: {} / {}".format(accuracy,
            len(accuracy_data[1])))
        return accuracy

    def save_model(self,filepath):
        all_attri = [a for a in dir(self) if not a.startswith('__') \
                and not callable(getattr(self,a)) \
                and not repr(getattr(self,a)).startswith('<') \
                and not a == 'activations' \
                and not a == 'quantity_idx_dict' \
                and not a == 'quantized_actv']
        string_wanted=''
        for i in all_attri:
            string_wanted=string_wanted+i+'=self.'+i+','
        string_wanted=string_wanted[:-1]
        exec('np.savez(filepath,'+string_wanted+')')
        self.model.save(filepath+'.h5')

    def load_model(self,filepath):
        npzfile = np.load(filepath+'.npz')
        all_attri =npzfile.files
        for i in all_attri:
            exec('self.'+i+"=npzfile['"+i+"']")
        self.model = load_model(filepath+'.h5')
        self.hid_actv = str(self.hid_actv)
        self.out_actv = str(self.out_actv)
        self.sgd = optimizers.SGD(lr=self.lr, decay=0.0, momentum=0.0, nesterov=False)
        try:
            self.load_quantity_idx_dict()
        except:
            pass



# Called by other Functions
    def load_model_from_python2(self,filepath, folder = "models/"):
        filepath = folder + filepath
        npzfile = np.load(filepath+'.npz',encoding = 'latin1')
        all_attri =npzfile.files
        for i in all_attri:
            exec('self.'+i+"=npzfile['"+i+"']")
        self.model = load_model(filepath+'.h5')
        self.sgd = optimizers.SGD(lr=self.lr, decay=0.0, momentum=0.0, nesterov=False)
        """npzfile.files
        np.savez(filepath,*all_attri)"""

    def calculate_activation(self, training_data):
        self.activations = [np.zeros((len(training_data[1]), y)) \
                for y in self.sizes[:]]
        self.quantized_actv = [np.zeros((len(training_data[1]), y),dtype = int) \
                for y in self.sizes[:]]
        for layer_idx in range(1,len(self.sizes)-1):
            print("calculating the activations of hidden layer {} ...".format(layer_idx))
            model_ = Sequential()
            for i in range(layer_idx):
                if i == len(self.sizes)-2:
                    model_.add(Dense(self.sizes[i+1], activation=self.out_actv,
                        input_dim=self.sizes[i],
                        weights=self.model.layers[i].get_weights()))
                else:
                    model_.add(Dense(self.sizes[i+1], activation=self.hid_actv,
                        input_dim=self.sizes[i],
                        weights=self.model.layers[i].get_weights()))
            activations= np.asfarray(model_.predict(training_data[0]),dtype = np.float64)
            self.activations[layer_idx] = activations
            self.quantized_actv[layer_idx] = quantize(activations,quanConst
                    = self.quan_const, sepaVec = self.sepaVec)

    def delete_node(self,layer_idx,node_list):
        """node pruning function, given the layer idx and node idx
        only the node in the hidden layer can be pruned"""
        self.sizes[layer_idx]= self.sizes[layer_idx]-len(node_list)
        model_ = Sequential()
        for i in range(len(self.sizes)-1):
            if i == len(self.sizes)-2: #this is the output layer
                actv_str = self.out_actv
            else:  #this is the hidden layer
                actv_str = self.hid_actv

            if i == layer_idx-1:
                temp_weight1,temp_biases1=self.model.layers[i].get_weights()
                temp_weight1 = np.delete(temp_weight1, node_list, axis=1)
                temp_biases1=np.delete(temp_biases1,node_list,axis=0)
                model_.add(Dense(self.sizes[i+1], activation=actv_str,
                    input_dim=self.sizes[i],weights=[temp_weight1,temp_biases1]))
            elif i == layer_idx:
                temp_weight2,temp_biases2=self.model.layers[i].get_weights()
                temp_weight2 = np.delete(temp_weight2, node_list, axis=0)
                model_.add(Dense(self.sizes[i+1], activation=actv_str,
                    input_dim=self.sizes[i],weights=[temp_weight2,temp_biases2]))
            else:
                model_.add(Dense(self.sizes[i+1], activation=actv_str,
                    input_dim=self.sizes[i],
                    weights=self.model.layers[i].get_weights()))

        model_.compile(optimizer=optimizers.Adadelta(),
                loss='sparse_categorical_crossentropy',metrics=['accuracy'])
        self.model = model_

    def delete_node_with_bias(self,layer_idx,node_list,
                              data_dependent_update = True,
                              bounded = False):
        """node pruning function, given the layer idx and node idx
        only the node in the hidden layer can be pruned
        """
        model_ = Sequential()
        for i in range(len(self.sizes)-1):
            if i == len(self.sizes)-2: #this is the output layer
                actv_str = self.out_actv
            else:  #this is the hidden layer
                actv_str = self.hid_actv

            if i == layer_idx-1:
                temp_weight1,temp_biases1=self.model.layers[layer_idx-1].get_weights()
                temp_weight1 = np.delete(temp_weight1, node_list, axis=1)
                pruned_bias = temp_biases1
                temp_biases1=np.delete(temp_biases1,node_list,axis=0)
                model_.add(Dense(self.sizes[i+1]-len(node_list), activation=actv_str,
                    input_dim=self.sizes[i],weights=[temp_weight1,temp_biases1]))
            elif i == layer_idx:
                temp_weight2,temp_biases2=self.model.layers[layer_idx].get_weights()
                if data_dependent_update:
                    for j in range(self.sizes[layer_idx+1]):
                        temp = 0
                        for k in node_list:
                            temp = temp +self.distribution_mean[layer_idx][k] \
                            * temp_weight2[k][j]
                        if bounded:
                            temp_biases2[j] = temp_biases2[j] + np.clip(temp,-2,2)
                        else:
                            temp_biases2[j] = temp_biases2[j] + temp
                else:
                    for k in node_list:
                        for j in range(self.sizes[layer_idx+1]):
                            temp_biases2[j] = temp_biases2[j] + pruned_bias[k] \
                            * temp_weight2[k][j]
                self.sizes[layer_idx]= self.sizes[layer_idx] - len(node_list)
                temp_weight2 = np.delete(temp_weight2, node_list, axis=0)
                model_.add(Dense(self.sizes[i+1], activation=actv_str,
                    input_dim=self.sizes[i],weights=[temp_weight2,temp_biases2]))
            else:
                model_.add(Dense(self.sizes[i+1], activation=actv_str,
                    input_dim=self.sizes[i],
                    weights=self.model.layers[i].get_weights()))
        model_.compile(optimizer=optimizers.Adadelta(),
            loss='sparse_categorical_crossentropy',metrics=['accuracy'])
        self.model = model_

    def calculate_kl_divergence(self, training_data):
        self.kl_diverge = [np.zeros((y, self.sizes[-1], self.sizes[-1])) \
                for y in self.sizes[:]]
        for layer_idx in range(1,len(self.sizes)-1):
            for node_idx in range(self.sizes[layer_idx]):
                for label_idx_i in range(self.sizes[-1]):
                    for label_idx_j in range(self.sizes[-1]):
                        self.kl_diverge[layer_idx][node_idx, label_idx_i, label_idx_j] \
                            =kl_divergence(self.cond_distribution[layer_idx] \
                            [node_idx,label_idx_i,:], self.cond_distribution \
                            [layer_idx][node_idx, label_idx_j,:])

    def load_quantity_idx_dict(self):
        self.quantity_idx_dict = {1 : self.entropy,
                             2 : self.mutual_information,
                             3 : self.max_entropy,
                             4 : self.quantity4,
                             5 : self.max_labeled_mutual_information,
                             6 : self.output_norm,
                             7 : self.random_group,
                             8 : self.max_kl_diverge,
                             9 : self.labeled_min_entropy,
                             10: self.max_labeled_kl_div,
                             11: self.max_JS_diverge,
                             12: self.naive_max_JS_diverge,
                             13: self.max_cond_joint_kl_div,
                             }

    def calculate_JS_divergence(self, training_data):
        self.max_JS_diverge = [np.zeros((y)) for y in self.sizes[:]]
        self.naive_max_JS_diverge = [np.zeros((y)) for y in self.sizes[:]]
        comb_count = np.full(self.sizes[-1], 2)
        self.JS_diverge = [np.zeros((y, *comb_count)) \
                for y in self.sizes[:]]
        self.naive_JS_diverge = [np.zeros((y, *comb_count)) \
                for y in self.sizes[:]]
        self.max_JS_diverge_index = [np.zeros((y, self.sizes[-1])) \
                for y in self.sizes[:]]
        self.naive_max_JS_diverge_index = [np.zeros((y, self.sizes[-1])) \
                for y in self.sizes[:]]
        for layer_idx in range(1,len(self.sizes)-1):
            for node_idx in range(self.sizes[layer_idx]):
                for comb in product(range(2), repeat = self.sizes[-1]):
                    P_in_A = np.inner(comb, self.data_distribution)
                    P_A = np.array([P_in_A, 1 - P_in_A])
                    inset, notinset = \
                        self.get_label_dependent_dist(comb, self.cond_distribution \
                        [layer_idx][node_idx,:,:])
                    P_T = self.distribution[layer_idx][node_idx,:]
                    self.JS_diverge[layer_idx][(node_idx, *comb)] \
                        = self.JS_diverge_func(inset, notinset,P_A = P_A, P_T = P_T)
                    self.naive_JS_diverge[layer_idx][(node_idx, *comb)] \
                        = self.JS_diverge_func(inset, notinset, naive = True)

                self.JS_diverge[layer_idx][(node_idx,*np.full(self.sizes[-1],1))] = 0
                self.JS_diverge[layer_idx][(node_idx,*np.full(self.sizes[-1],0))] = 0
                self.naive_JS_diverge[layer_idx][(node_idx,*np.full(self.sizes[-1],1))] = 0
                self.naive_JS_diverge[layer_idx][(node_idx,*np.full(self.sizes[-1],0))] = 0
                self.max_JS_diverge[layer_idx][node_idx] \
                    = np.amax(self.JS_diverge[layer_idx][node_idx, :])
                self.naive_max_JS_diverge[layer_idx][node_idx] \
                    = np.amax(self.naive_JS_diverge[layer_idx][node_idx, :])
                #find the fisrt position of the maximum value
                self.max_JS_diverge_index[layer_idx][node_idx, :] \
                    = np.argwhere(self.JS_diverge[layer_idx][node_idx, :] \
                    == self.JS_diverge[layer_idx][node_idx, :].max())[0]
                self.naive_max_JS_diverge_index[layer_idx][node_idx, :] \
                    = np.argwhere(self.naive_JS_diverge[layer_idx][node_idx, :] \
                    == self.naive_JS_diverge[layer_idx][node_idx, :].max())[0]

    def calculate_KL_cond_joint_divergence(self, training_data):
        self.max_cond_joint_kl_div = [np.zeros((y)) for y in self.sizes[:]]
        comb_count = np.full(self.sizes[-1], 2)
        self.cond_joint_kl_div = [np.zeros((y, *comb_count)) \
                for y in self.sizes[:]]
        self.max_cond_joint_kl_div_index = [np.zeros((y, self.sizes[-1])) \
                for y in self.sizes[:]]
        for layer_idx in range(1,len(self.sizes)-1):
            for node_idx in range(self.sizes[layer_idx]):
                for comb in product(range(2), repeat = self.sizes[-1]):
                    inset, notinset = \
                        self.get_label_dependent_dist(comb, self.cond_distribution \
                        [layer_idx][node_idx,:,:])
                    P_T = self.distribution[layer_idx][node_idx,:]
                    self.cond_joint_kl_div[layer_idx][(node_idx, *comb)] \
                        = kl_divergence(inset, P_T)

                self.cond_joint_kl_div[layer_idx][(node_idx,*np.full(self.sizes[-1],1))] = 0
                self.cond_joint_kl_div[layer_idx][(node_idx,*np.full(self.sizes[-1],0))] = 0
                self.max_cond_joint_kl_div[layer_idx][node_idx] \
                    = np.amax(self.cond_joint_kl_div[layer_idx][node_idx, :])
                #find the fisrt position of the maximum value
                self.max_cond_joint_kl_div_index[layer_idx][node_idx, :] \
                    = np.argwhere(self.cond_joint_kl_div[layer_idx][node_idx, :] \
                    == self.cond_joint_kl_div[layer_idx][node_idx, :].max())[0]

    def get_label_dependent_dist(self, label, cond_distribution):
        inv_label  = [- bool_term+1 for bool_term in label]
        inset = np.multiply(label, self.data_distribution)
        inset = np.inner(inset, np.transpose(cond_distribution))
        notinset = np.multiply(inv_label, self.data_distribution)
        notinset = np.inner(notinset, np.transpose(cond_distribution))
        inset = inset /(sum(inset))
        notinset = notinset /(sum(notinset))
        return inset, notinset

    def JS_diverge_func(self, inset, notinset, naive = False, P_A = None, P_T = None):
        if naive:
            P_T = 0.5 * inset + 0.5 * notinset
            result_1_part = 0.5 * kl_divergence(inset, P_T)
            result_2_part = 0.5 * kl_divergence(notinset, P_T)
        else:
            result_1_part = P_A[0] * kl_divergence(inset, P_T)
            result_2_part = P_A[1] * kl_divergence(notinset, P_T)
        return result_1_part + result_2_part


# Other Features
    def calculate_conditional_entropy(self):
        needed_size = self.sizes[:]
        needed_size[0],needed_size[-1] = 0, 0
        self.conditional_entropy_matrix = [np.zeros((y,y)) for y in needed_size[:]]
        for layer_idx in range(1,len(self.sizes)-1):
            for node_idx_i in range(self.sizes[layer_idx]):
                for node_idx_j in range(self.sizes[layer_idx]):
                    if node_idx_i == node_idx_j:
                        self.conditional_entropy_matrix[layer_idx][node_idx_i,node_idx_j] = 1
                    else:
                        i = self.activations[layer_idx][:,node_idx_i]<0.5
                        j = self.activations[layer_idx][:,node_idx_j]<0.5
                        self.conditional_entropy_matrix[layer_idx][node_idx_i,node_idx_j] = cond_entropy_func(i,j)
        self.conditional_entropy_matrix = 0

    def calculate_weight_similarity(self):
        self.M_matrix = [np.zeros((y,y)) for y in self.sizes[:]]
        for layer_idx in range(1,len(self.sizes)-1):
            weight_in = self. model.layers[layer_idx-1].get_weights()[0]
            weight_out = self. model.layers[layer_idx].get_weights()[0]
            for i in range(self.sizes[layer_idx]):
                for j in range(self.sizes[layer_idx]):
                    aj = np.mean(np.power(weight_out[j,:],2))
                    self.M_matrix[layer_idx][i,j] = aj*LA.norm(weight_in[:,i]-weight_in[:,j],2)

    def merge_node(self,layer_idx,node_number,surgery = True,
            validation_data=None):
        accu = []
        for i in range(node_number):
            self.calculate_weight_similarity()
            min_idx = [0,1]
            temp = self.M_matrix[layer_idx][0,1]
            for row_idx in range(self.sizes[layer_idx]):
                for column_idx in range(self.sizes[layer_idx]):
                    if self.M_matrix[layer_idx][row_idx,column_idx] < temp \
                            and row_idx != column_idx:
                        min_idx = [row_idx, column_idx]
                        temp = self.M_matrix[layer_idx][row_idx,column_idx]
            model_ = Sequential()
            for i in range(len(self.sizes)-1):
                if i == len(self.sizes)-2: #this is the output layer
                    actv_str = self.out_actv
                else:  #this is the hidden layer
                    actv_str = self.hid_actv

                if i == layer_idx-1:
                    temp_weight1,temp_biases1=self.model.layers[layer_idx-1].get_weights()
                    temp_weight1 = np.delete(temp_weight1, min_idx[1], axis=1)
                    temp_biases1=np.delete(temp_biases1,min_idx[1],axis=0)
                    model_.add(Dense(self.sizes[i+1]-1, activation=actv_str,
                        input_dim=self.sizes[i],weights=[temp_weight1,temp_biases1]))
                elif i == layer_idx:
                    temp_weight1,temp_biases1=self.model.layers[layer_idx].get_weights()
                    if surgery:
                        temp_weight1[min_idx[0],:] = temp_weight1[min_idx[0],:]\
                        +temp_weight1[min_idx[1],:]
                    temp_weight1 = np.delete(temp_weight1, min_idx[1], axis=0)
                    model_.add(Dense(self.sizes[i+1], activation=actv_str, input_dim=self.sizes[i]-1,weights=[temp_weight1,temp_biases1]))
                else:
                    model_.add(Dense(self.sizes[i+1], activation=actv_str,
                        input_dim=self.sizes[i],
                        weights=self.model.layers[i].get_weights()))
            self.model = model_
            self.sizes[layer_idx]= self.sizes[layer_idx]-1
            self.model.compile(optimizer=optimizers.Adadelta(),
                    loss='sparse_categorical_crossentropy',metrics=['accuracy'])
            lost,accuracy=self.model.evaluate(validation_data[0],
                    validation_data[1])
            accu.append(accuracy)
        print(accu)
        return accu

    def remove_redundant_node(self,layer_idx,node_number,do_bias_balance = True):
        for i in range(node_number):
            cond_entropy = np.amin(self.conditional_entropy_matrix[layer_idx],axis = 1)
            selected_node_idx = np.argmin(cond_entropy)
            model_ = Sequential()
            for i in range(len(self.sizes)-1):
                if i == layer_idx-1:
                    temp_weight1,temp_biases1=self.model.layers[layer_idx-1].get_weights()
                    temp_weight1 = np.delete(temp_weight1, selected_node_idx, axis=1)
                    temp_biases1=np.delete(temp_biases1,selected_node_idx,axis=0)
                    model_.add(Dense(self.sizes[i+1]-1, activation='sigmoid', input_dim=self.sizes[i],weights=[temp_weight1,temp_biases1]))
                elif i == layer_idx:
                    temp_weight1,temp_biases1=self.model.layers[layer_idx].get_weights()
                    if do_bias_balance:
                        for j in range(self.sizes[layer_idx+1]):
                            temp_biases1[j] = temp_biases1[j] + self.distribution_mean[layer_idx][
                                selected_node_idx]*temp_weight1[selected_node_idx][j]
                    temp_weight1 = np.delete(temp_weight1, selected_node_idx, axis=0)
                    model_.add(Dense(self.sizes[i+1], activation='sigmoid', input_dim=self.sizes[i]-1,weights=[temp_weight1,temp_biases1]))
                else:
                    model_.add(Dense(self.sizes[i+1], activation='sigmoid', input_dim=self.sizes[i],weights=self.model.layers[i].get_weights()))
            self.model = model_
            self.sizes[layer_idx]= self.sizes[layer_idx]-1
            self.distribution_mean[layer_idx]=np.delete(self.distribution_mean[layer_idx],selected_node_idx,axis=0)
            self.conditional_entropy_matrix[layer_idx] = np.delete(self.conditional_entropy_matrix[layer_idx], selected_node_idx, axis=1)
            self.conditional_entropy_matrix[layer_idx] = np.delete(self.conditional_entropy_matrix[layer_idx], selected_node_idx, axis=0)
        self.model.compile(optimizer=self.sgd,loss='sparse_categorical_crossentropy',metrics=['accuracy'])

    def calculate_histogram(self):
        self.kl_separation_count = [np.zeros(self.sizes[-1]) \
                for y in self.sizes[:]]
        self.naive_kl_separation_count = [np.zeros(self.sizes[-1]) \
                for y in self.sizes[:]]
        for layer_idx in range(1,len(self.sizes)-1):
            for node_idx in range(self.sizes[layer_idx]):
                set_size = countset(self.max_JS_diverge_index[layer_idx][node_idx])
                self.kl_separation_count[layer_idx][set_size] += 1
                set_size_naive = countset(
                    self.naive_max_JS_diverge_index[layer_idx][node_idx])
                self.naive_kl_separation_count[layer_idx][set_size_naive] += 1

    def calculate_distribution(self,training_data):
        self.distribution = [np.zeros((y,self.quan_const)) for y in self.sizes[:]]
        for layer_idx in range(1,len(self.sizes)-1):
            for node_idx in range(self.sizes[layer_idx]):
                self.distribution[layer_idx][node_idx,:] \
                    =vec2distr(self.quantized_actv[layer_idx][:,node_idx],
                               quanConst = self.quan_const)




modulename = 'cythonized.math_utils'
if modulename in sys.modules:
    print("The Cython Implementation for math_utils is currently used")
else:
    print("The Python Implementation for math_utils is currently used")

def status_bar(current_idx, task_length, task = "epochs", extra_log = '' ):
            progressLength = 50
            sys.stdout.write("training ["+"-" * (progressLength* current_idx//task_length)
                + " " * (progressLength - (progressLength * current_idx // task_length))
                + "]" + str(current_idx)+"/"+str(task_length)+ ' ' + task + ' '
                + str(extra_log) + "\r")
            sys.stdout.flush()

def cond_entropy_func(i,j):
    """ import numpy as np
    from math import log
    a = np.array([1,0,0])
    a = a==1
    b = np.array([0,1,0])
    b= b==1
    cond_entropy_func(a,b)

    """
    data_length = float(len(i))
    Pj1 = len(np.where(j == True)[0])/data_length
    Pj0 = len(np.where(j == False)[0])/data_length
    Pi1j1 = len(np.where(i[np.where(j==True)[0]]==True)[0])
    Pi0j1 = len(np.where(i[np.where(j==True)[0]]==False)[0])
    if (Pi1j1+Pi0j1) ==0:
        [Pi1j1, Pi0j1] = [0.5,0.5]
    else:
        Pi1j1, Pi0j1 = float(Pi1j1)/(Pi1j1+Pi0j1), float(Pi0j1)/(Pi1j1+Pi0j1)
    Pi1j0 = len(np.where(i[np.where(j==False)[0]]==True)[0])
    Pi0j0 = len(np.where(i[np.where(j==False)[0]]==False)[0])
    if (Pi1j0+Pi0j0) ==0:
        [Pi1j0, Pi0j0] = [0.5,0.5]
    else:
        Pi1j0, Pi0j0 = float(Pi1j0)/(Pi1j0+Pi0j0), float(Pi0j0)/(Pi1j0+Pi0j0)
    return entropy_([Pi1j1, Pi0j1],2)*Pj1 + entropy_([Pi1j0, Pi0j0],2)*Pj0

def entropy_(p,precision):
    """entropy function given distribution and quantization bin numbers"""
    z = 0
    for number in p:
        if number > 0:
            z = z-number*log(number,2)
    return z

def countset(set_indicator):
    length = len(set_indicator)
    nonzero = int(sum(set_indicator))
    if nonzero > length/2:
        nonzero = length - nonzero
    return nonzero
