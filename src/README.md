# README
### All the Dependencies of the Codes
When working with a ubuntu machine, these are the codes needed to install of the dependent packages
```bash
sudo apt install python3-pip
pip3 install jupyter --user
pip3 install ipython --user
ipython3 kernel install --user
pip3 install numpy --user
pip3 install keras --user
pip3 install tensorflow --user
pip3 install h5py --user
pip3 install PySide --user
pip3 install matplotlib --user
pip3 install cython --user
```
### Open the Jupyter Notebook and Work with the Code
When all the dependencies are resolved, open the terminal in current folder and open a jupyter local server by entering

```bash
jupyter nodebook
```
open the jupyter file `Neural Network Pruning.ipynb`. This file gives the fundamentary introduction of the codes.
Press <kbd>Shift</kbd>+<kbd>Enter</kbd> to excute the code in the current cell and move to the next cell.

Open the jupyter file `Show Results of Simulations.ipynb`. This file loads the simulation data and plots the results of it.

### Files Description

- `keras_network.py` contains the definition of class `keras_network.Network`.
This class instantiate a neural network model. One can train this model, calculating the information theoretic quantities of it and do the pruning. The model can be saved and loaded from data as well.

- `my_plot.py` contains the functions to generate plot from data

- `network_utils.py` contains the functions to the pruning on the neural network systematically.

- `utils.py` implements some utility functions

- `loaders/` folder contains the python scripts to load the dataset

- `models/` folder contains the saved models, including the weight matrix and the corresponding information theoretic quantities.

- `dataset/` folder contains the datasets file loaded by the loader files

- `results/` folder contains the pruning accuracy drop matrices, which will be loaded to generate the plot result
