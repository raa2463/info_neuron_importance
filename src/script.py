#1. prune a trained NN for MNIST
import network_utils
from loaders import mnist_loader
data_list = mnist_loader.load_data()
[y1,y2,y3,x] = network_utils.pruning_layerwise('784_30_30_10_2', 1, node_number = 1,
               data = data_list, quantity_list = [1,2,7])

#2. prune a trained NN for LIR
import network_utils
from loaders import lir_loader
data_list = lir_loader.load_data()
[y1,y2,y3,x] = network_utils.pruning_layerwise('16_30_30_26_1', 1, node_number = 20,
               data = data_list, quantity_list = [2], do_retraining = True)


from my_plot import plot_pruning_curve
import matplotlib.pyplot as plt
fig, ax = plt.subplots()
plot_pruning_curve([y1,y2,y3,x], fig, ax, do_retrain = True, quantity_list = [2])


#3. get pruning accuracy matrix for a trained NN
import network_utils
from loaders import mnist_loader
data_list = mnist_loader.load_data()
[y1,y2,y3,x] = network_utils.pruning_layers_evenly('784_30_30_10_2',node_number = 2,
               data = data_list, quantity_list = [2,7])


#4. get pruning accuracy matrix for different size NN
import network_utils
from loaders import mnist_loader
data_list = mnist_loader.load_data()
[y, x] = network_utils.fresh_training([784,30,30,10], node_number = 15,
        data = data_list, epoch = 3)



#5. get animation of weight matrix where the weights corresponding
# to the most redundant 13 nodes is highlighted
from my_animation import shading_matrix
import matplotlib.pyplot as plt
fig, ax = plt.subplots()
ani = shading_matrix('16_30_30_26_2',fig, ax, node_number = 13, choose_quantity = 0)
plt.show()



#6. get label dependent accuracy drop matrix
import network_utils
from loaders import mnist_loader
data_list = mnist_loader.load_data()
y = network_utils.label_dependent_pruning('784_30_30_10_2',layer_index = 1,
    node_number = 13, data = data_list, choosen_quantity = 2, do_bias_balance = True)



#7. using the y matrix in the previous code block and plot it
from my_plot import plot_accu_mat
import matplotlib.pyplot as plt
fig, ax = plt.subplots()
plot_accu_mat(y,fig,ax)
plt.show()




#8. show thee node ranking table aside the weight matrix
from my_plot import plot_node_rank
import matplotlib.pyplot as plt
fig, ax = plt.subplots()
plot_node_rank('16_30_30_26_2', fig, ax, reranking = True, rerank_quantity = 0)
plt.show()




#9. plot the distribution of certain node
import numpy as np
import matplotlib.pyplot as plt
import keras_network
import my_plot
net = keras_network.Network([])
net.load_model('../quantization_20/models/16_30_30_26_1')#20 bin quantizer
distri_data = net.cond_distribution[1][0][9] #distri_data = net.distribution[1][1]
fig, ax = plt.subplots()
my_plot.plot_1D_distribution(distri_data, fig, ax)
plt.show()




#10. plot the prediction difference of five trained models
from network_utils import models_prediction_comparison
from loaders import mnist_loader
data_list = mnist_loader.load_data()
diff_mat = models_prediction_comparison(data = data_list)
print(diff_mat)





#11. plot node quantities comparison
import matplotlib.pyplot as plt
from my_plot import quantities_comparison
fig, ax = plt.subplots()
quantities_comparison('16_30_30_26_1', fig, ax)
plt.show()

import network_utils
from loaders import mnist_loader
data_list = mnist_loader.load_data()
[y1,y2,y3,x] = network_utils.pruning_layerwise('784_100_100_10_1', 1, node_number = 80,
               data = data_list, quantity_list = [2], do_retraining = True)





import keras_network




