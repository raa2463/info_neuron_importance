from distutils.core import setup
from Cython.Build import cythonize
import numpy as np



setup(
    ext_modules = cythonize("cythonized/*.pyx"),
    include_dirs = [np.get_include()],
)


"""
python3 setup.py build_ext --inplace
"""
